function eta = overpotential(Phi_s, Phi_e, U_eq, flux, film, param)
    if param.EnableAgeing == 1
        eta = Phi_s - Phi_e - U_eq ...
            - param.F.*flux.*(param.R_SEI+film./(param.k_n_plated_Li));
    else
        eta = Phi_s - Phi_e - U_eq;
    end
end