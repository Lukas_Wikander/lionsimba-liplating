%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code was written by Marcello Torchio, University of Pavia.
% Please send comments or questions to
% marcello.torchio01@ateneopv.it
%
% Copyright 2017: 	Marcello Torchio, Lalo Magni, and Davide M. Raimondo, University of Pavia
%					Bhushan Gopaluni, University of British Columbia
%                 	Richard D. Braatz, MIT.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IONICFLUX computes the value of the ionic flux in the electrodes. [mol /
% (m^2*s)]
function [jflux,U_p,U_n,dudt_p,dudt_n,J_s] = ionicFlux(ce,cs_star,Phis,Phie,T,solverFlux,solverSideFlux,film,param)

%% Positive electrode
% Compute the OCV for the positive and negative electrodes.
% [U_p,dudt_p,U_n,dudt_n] = param.OpenCircuitPotentialFunction(cs_star,T,param);
[U_p,dudt_p,U_n,dudt_n] = openCircuitPotential(cs_star,T,param);
% Compute the reaction rates.
[k_pT, k_nT, k_plT] = param.ReactionRatesFunction(T,param);

% Positive electrode ion flux
deltap = ((0.5*param.F)./(param.R*T(param.Nal+1:param.Nal+param.Np))).*(Phis(1:param.Np)-Phie(1:param.Np)-U_p);
ip = 2*k_pT.*sqrt(ce(1:param.Np)).*sqrt(cs_star(1:param.Np)).*sqrt(param.cs_maxp-cs_star(1:param.Np));
jnp_calc = ip.* sinh(deltap);

%% Negative electrode

% If ageing is enabled, take into account the SEI resistance
if(param.EnableAgeing==1)
    eta_n   = (Phis(param.Np+1:end)-Phie(param.Np+param.Ns+1:end)-U_n -param.F*solverFlux(param.Np+1:end).*(param.R_SEI+film./(param.k_n_plated_Li)));
else
    eta_n   = (Phis(param.Np+1:end)-Phie(param.Np+param.Ns+1:end)-U_n -param.F*solverFlux(param.Np+1:end).*(param.R_SEI));
end


deltan      = ((0.5*param.F)./(param.R*T(param.Nal+param.Np+param.Ns+1:param.Nal+param.Np+param.Ns+param.Nn))).*eta_n;
in          = 2.*k_nT.*sqrt(ce(param.Np+param.Ns+1:end)).*sqrt(cs_star(param.Np+1:end)).*sqrt(max(param.cs_maxn-cs_star(param.Np+1:end),0));

jnn_calc    = in.* sinh(deltan);

J_s = zeros(param.Nn,1);

% Switch the cases in which the appplied current density is a symbolical variable
if(isa(param.I,'casadi.SX') && param.EnableAgeing == 1)
	eta_s = Phis(param.Np+1:end) - Phie(param.Np+param.Ns+1:end) ...
        - param.Uref_s ...
        - param.F*solverSideFlux(param.Np+1:end).*(param.R_SEI+film./(param.k_n_plated_Li));
    % Butler-Volmer equation for the side reaction flux
    expon = param.F ./ (param.R .* T(param.Nal+param.Np+param.Ns+1:end-param.Nco));
    % Reaction equilibrium
    i_side_0 = param.F .* param.k_pl .* ce(param.Np+param.Ns+1:end) .^ param.alpha_pl_a;
    
    % By means of the if_else statement of CasADi, it is possible to represent dynamics that switch according to the value of the sumbolical quantity param.I
    J_s = if_else(param.I>=0, 2.*i_side_0./param.F.*( exp(param.alpha_pl_a.*expon.*eta_s) - exp(-param.alpha_pl_c.*expon.*eta_s) ), zeros(param.Nn,1));
elseif(param.EnableAgeing == 1 && param.I > 0)
    
    eta_s = overpotential(Phis(param.Np+1:end),...
        Phie(param.Np+param.Ns+1:end),...
        param.Uref_s,...
        solverSideFlux(param.Np+1:end),...
        film,...
        param);
    
    % Butler-Volmer equation for the side reaction flux
    expon = param.F ./ (param.R .* T(param.Nal+param.Np+param.Ns+1:end-param.Nco));
    % Reaction equilibrium (simplified)
    i_side_0 = param.F .* k_plT .* ce(param.Np+param.Ns+1:end) .^ param.alpha_pl_a;
    % Reaction equilibrium
    %i_side_0 = param.F .* k_plT .* ce(param.Np+param.Ns+1:end).^param.alpha_pl_a .* cs_star(param.Np+1:end).^param.alpha_pl_c .* (param.cs_maxn-cs_star(param.Np+1:end)).^param.alpha_pl_a;
    % Limit flux to only be negative
    J_s = min(0, i_side_0./param.F.*( exp(param.alpha_pl_a.*expon.*eta_s) - exp(-param.alpha_pl_c.*expon.*eta_s) ));
end
%% Return value
jflux = [jnp_calc;jnn_calc];
